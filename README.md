![Build Status](https://gitlab.com/jclarkin/analog-clock/badges/master/build.svg)

---

Simple CSS + JS animated clock

[Click Here to see](https://jclarkin.gitlab.io/analog-clock/)
