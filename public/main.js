(() => {
  window.setInterval(() => {
    const now = new Date();

    const hours = ((now.getHours()/12 + now.getMinutes()/(12*60) + now.getSeconds()/(12*60*60)) * 360) + 90;
    const handHours = window.document.querySelector('.hand-hour');
    handHours.style.transform = `rotate(${hours}deg)`;

    const minutes = ((now.getMinutes()/60 + now.getSeconds()/(60*60)) * 360) + 90;
    const handMinutes = window.document.querySelector('.hand-minute');
    handMinutes.style.transform = `rotate(${minutes}deg)`;

    const seconds = (now.getSeconds()/60 * 360) + 90;
    const handSeconds = window.document.querySelector('.hand-second');
    handSeconds.style.transform = `rotate(${seconds}deg)`;
  }, 1000);
})();
